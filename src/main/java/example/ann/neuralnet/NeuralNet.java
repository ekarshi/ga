package example.ann.neuralnet;


import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.math3.analysis.function.Sigmoid;
import org.apache.commons.math3.analysis.function.Tanh;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 * Feed forward neural network only forward pass allowed
 * The optimized weights would be set by the Genetic Algorithm
 */
public class NeuralNet {
    private final int inputNeurons;
    private final int hiddenNeurons;
    private final int outNeurons;
    private final RealMatrix hiddenWeightMatrix;
    private final RealMatrix outWeightMatrix;
    private final Array2DRowRealMatrix array2DRowRealMatrix;
    private final RealVector inputBias;
    private final RealVector hiddenBias;
    private Sigmoid sigmoid = new Sigmoid();
    private Tanh tanhActivation = new Tanh();

    public NeuralNet(int inputNeurons, int hiddenNeurons, int outNeurons) {
        this.inputNeurons = inputNeurons;
        this.hiddenNeurons = hiddenNeurons;
        this.outNeurons = outNeurons;

        array2DRowRealMatrix = new Array2DRowRealMatrix();
        hiddenWeightMatrix = array2DRowRealMatrix.createMatrix(inputNeurons, hiddenNeurons);
        outWeightMatrix = array2DRowRealMatrix.createMatrix(hiddenNeurons, outNeurons);
        inputBias = new ArrayRealVector(hiddenNeurons);
        hiddenBias = new ArrayRealVector(outNeurons);
    }

    public void setUpWeights(Double[] weights) {
        weightSetUp(weights);
    }

    /**
     * Compute the network output given the data
     *
     * @param inputData
     * @return
     */
    public double[] netOut(double[] inputData) {
        RealVector input = new ArrayRealVector(inputData);
        RealVector summedInput = inputBias.add(hiddenWeightMatrix.preMultiply(input));
        summedInput = applyActivation(summedInput);
        RealVector outVector = hiddenBias.add(outWeightMatrix.preMultiply(summedInput));
        outVector = applyActivation(outVector);
        return outVector.toArray();
    }

    /**
     * Apply the activation function in this example case
     * RELU (Rectifier linear unit)
     *
     * @param vector
     * @return
     */
    private RealVector applyActivation(RealVector vector) {
        return new ArrayRealVector(vector.map(this::relu).toArray());
    }

    private int splitMatrix(int x, int y, int index, double[] weights, RealMatrix realMatrix) {
        for (int row = 0; row < x; row++) {
            for (int col = 0; col < y; col++) {
                realMatrix.setEntry(row, col, weights[index]);
                index += 1;
            }
        }
        return index;
    }

    /**
     * Set up the weights of the input and the hidden layers of the network
     *
     * @param weights
     */
    private void weightSetUp(Double[] weights) {
        setUpBias(weights);
        double[] weightsData = ArrayUtils.subarray(ArrayUtils.toPrimitive(weights), inputNeurons + outNeurons, weights.length);
        int index = splitMatrix(inputNeurons, hiddenNeurons, 0, weightsData, hiddenWeightMatrix);
        splitMatrix(hiddenNeurons, outNeurons, index, weightsData, outWeightMatrix);
    }

    /**
     * Set up the bias for the input and output layers
     * An essential ingredient for the performance of Neural Net
     *
     * @param weights
     */
    private void setUpBias(Double[] weights) {
        int internal = 0;
        for (int count = 0; count < hiddenNeurons + outNeurons; count++) {
            if (count < hiddenNeurons) {
                inputBias.setEntry(count, weights[count]);
                internal = count;
            } else
                hiddenBias.setEntry(count - (internal + 1), weights[count]);
        }
    }

    /**
     * RELU (Rectifier linear unit)
     *
     * @param data
     * @return
     */
    private double relu(double data) {
        return data > 0 ? data : 0;
    }
}
