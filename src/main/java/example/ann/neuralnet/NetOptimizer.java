package example.ann.neuralnet;

import example.ann.data.DataSet;
import example.ann.util.Utility;
import ga.algorithm.AbstractGA;
import ga.algorithm.AlgorithmConfig;
import ga.algorithm.IncrementalGA;
import ga.constants.CrossOverAlgorithms;
import ga.constants.MutationAlgorithms;
import ga.constants.SelectionAlgorithms;
import ga.fitness.Fitness;
import ga.model.Chromosome;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 * Optimize a feed forward neural network
 * Generate approximately 300 random solutions
 * Each solution is the network weight combined for the input layer and hidden layer
 * Used GaussianMutation and ConvexCombination Crossover and RouletteWheelSelection
 * GA type incremental
 * Criteria to stop is at 95% accuracy for the neural net
 */
public class NetOptimizer {
    private static final int INITIAL_SOLUTIONS = 300;

    private final AbstractGA<Chromosome<Double>, Double> abstractGA;
    private  NeuralNet neuralNet;
    private  DataSet dataSet;
    private final Double lowerBound = 0.0;
    private final Double upperBound = 1.0;
    /**
     * Fitness function for the problem domain
     * Forward pass the data through the neural net
     * Check the no. of patterns rightly classified
     * The accuracy of the network is then (patterns classified correctly / no of samples in the training set)
     * This fitness value is stored in the chromosome
     * The fitness score value is used later by the selection algorithm
     */
    private final Fitness<Chromosome<Double>, Double> fitness = chromosome -> {
        boolean[] recordsMatched = new boolean[dataSet.recordCount()];
        neuralNet.setUpWeights(chromosome.getGeneData());
        for (int i = 0; i < dataSet.recordCount(); i++) {
            int prediction = Utility.maxIndex(neuralNet.netOut(dataSet.getTrainRecord(i)));
            recordsMatched[i] = dataSet.matchTrainRecord(i, prediction);
        }
        double fitnessScore = IntStream.range(0, recordsMatched.length).filter(i -> recordsMatched[i]).boxed().count() / (double) recordsMatched.length;
        chromosome.setFitnessValue(fitnessScore);
    };
    /**
     * The criteria to stop is if the network can classify the samples with a accuracy > 95%
     */
    private final Predicate<Chromosome<Double>> solution = chromosome -> chromosome.getFitnessValue() > 0.95;

    /**
     * Set up the neural net and configure the algorithm
     *
     * @param dataSet
     * @param inputNeurons
     * @param outNeurons
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public NetOptimizer(DataSet dataSet, int inputNeurons, int outNeurons) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        this.dataSet = dataSet;
        int hiddenNeurons = inputNeurons + 3;
        neuralNet = new NeuralNet(inputNeurons, hiddenNeurons, outNeurons);
        int geneLength = hiddenNeurons + outNeurons + (inputNeurons * hiddenNeurons) + (hiddenNeurons * outNeurons);
        SolutionGenerator solutionGenerator = new SolutionGenerator(geneLength);
        AlgorithmConfig<Chromosome<Double>, Double> algorithmConfig = new AlgorithmConfig.Builder<Chromosome<Double>, Double>()
                .fitnessFunction(fitness)
                .initialChromosomes(solutionGenerator::generateRandomSolutions)
                .crossOverAlgorithm(CrossOverAlgorithms.CONVEX_COMBINATION_CROSSOVER, lowerBound, upperBound, 1.0)
                .mutationAlgorithm(MutationAlgorithms.GAUSSIAN_MUTATION, lowerBound, upperBound, 0.5)
                .selectionAlgorithm(SelectionAlgorithms.STOCHASTIC_UNIVERSAL_SAMPLING)
                .solution(solution)
                .build();
        abstractGA = new IncrementalGA<>(algorithmConfig);
    }

    public NeuralNet generateNetwork() {
        abstractGA.generate();
        neuralNet.setUpWeights(abstractGA.solution().getGeneData());
        return neuralNet;
    }

    /**
     * This inner class generates random initial
     * solutions to be supplied to the chromosome pool
     */
    private class SolutionGenerator {
        private final int geneLength;

        SolutionGenerator(int geneLength) {
            this.geneLength = geneLength;
        }

        private List<Chromosome<Double>> generateRandomSolutions() {
            List<Chromosome<Double>> chromosomeList = new ArrayList<>();
            for (int i = 0; i < INITIAL_SOLUTIONS; i++) {
                chromosomeList.add(new Chromosome<>(randomDouble(geneLength)));
            }
            return chromosomeList;
        }

        private Double[] randomDouble(int geneLength) {
            RandomDataGenerator generator = new RandomDataGenerator();
            Double[] genes = new Double[geneLength];
            for (int i = 0; i < geneLength; i++)
                genes[i] = generator.nextUniform(lowerBound, upperBound, true);
            return genes;
        }
    }
}
