package example.ann.data;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Consumes the raw csv file to create an in memory
 * representation of the data and normalizing it therefore.
 */
public class DataHandler {
    private static final Logger LOGGER = LogManager.getLogger(DataSet.class);
    private final int patternCol;
    private double[][] data;
    private int[] patterns;
    private final List<String> knownPatterns = new ArrayList<>();

    public DataHandler(String fileName, int colCount, int patternCol) {
        this.patternCol = patternCol - 1;
        List<String> fileContents = getFileContents(fileName);
        setUpDataDimensions(fileContents.size(), colCount - 1);
        fillUpDataHolders(fileContents);
    }

    public int getDataCount() {
        return data.length;
    }

    public int getDataLength() {
        return data[0].length;
    }

    public double[] getDataAtIndex(int index) {
        return data[index];
    }

    public int getPatternAtIndex(int index) {
        return patterns[index];
    }

    public String getPattern(int index) {
        return knownPatterns.get(index);
    }

    private List<String> getFileContents(String fileName) {
        try {
            List<String> lines = IOUtils.readLines(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(fileName)), "UTF-8");
            Collections.shuffle(lines);
            return lines;
        } catch (IOException e) {
            LOGGER.warn(e.getMessage());
        }
        return Collections.emptyList();
    }

    private void setUpDataDimensions(int rows, int cols) {
        data = new double[rows][cols];
        patterns = new int[rows];
    }

    private void fillUpDataHolders(List<String> fileContents) {
        for (int i = 0; i < fileContents.size(); i++)
            processLine(i, fileContents.get(i));
        NormalizeData.normalize(data);
    }

    private void processLine(int index, String line) {
        String[] split = line.split(",");
        for (int i = 0, j = 0; i < split.length; i++) {
            if (i == patternCol)
                processPattern(index, split[i]);
            else {
                data[index][j] = Double.parseDouble(split[i]);
                j += 1;
            }
        }
    }

    private void processPattern(int index, String pattern) {
        int indexOfPattern = knownPatterns.indexOf(pattern);
        if (indexOfPattern == -1) {
            knownPatterns.add(pattern);
            indexOfPattern = knownPatterns.size() - 1;
        }
        patterns[index] = indexOfPattern;
    }

}
