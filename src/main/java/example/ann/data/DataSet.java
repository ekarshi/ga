package example.ann.data;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.IntStream;

/**
 * In memory representation of the data split into test
 * and training set along with the associated patterns
 */
public class DataSet {
    private static final Logger LOGGER = LogManager.getLogger(DataSet.class);
    private double[][] testData;
    private int[] testDataPattern;
    private double[][] trainingData;
    private int[] trainingDataPattern;
    private final DataHandler dataHandler;
    private final double percentage;

    public DataSet(DataHandler dataHandler, double percentage) {
        this.dataHandler = dataHandler;
        this.percentage = percentage;
        splitDataSet();
    }

    public int recordCount() {
        return trainingData.length;
    }

    public double[] getTrainRecord(int index) {
        return trainingData[index];
    }

    public boolean matchTrainRecord(int index, int given) {
        return trainingDataPattern[index] == given;
    }

    public double[] getTestRecord(int index) {
        return testData[index];
    }

    public boolean matchTestRecord(int index, int given) {
        return testDataPattern[index] == given;
    }

    public int getTestPatternAtIndex(int index) {
        return testDataPattern[index];
    }

    public int testRecordCount() {
        return testData.length;
    }

    public String getPattern(int index) {
        return dataHandler.getPattern(index);
    }

    private void splitDataSet() {
        int countTrain = 0;
        int countTest = 0;
        int splitCount = (int) (dataHandler.getDataCount() * percentage);
        setDimensions(splitCount);
        int[] indexes = IntStream.range(0, dataHandler.getDataCount()).limit(splitCount).toArray();
        for (int i = 0; i < dataHandler.getDataCount(); i++) {
            if (!ArrayUtils.contains(indexes, i)) {
                trainingData[countTrain] = dataHandler.getDataAtIndex(i);
                trainingDataPattern[countTrain] = dataHandler.getPatternAtIndex(i);
                countTrain += 1;
            } else {
                testData[countTest] = dataHandler.getDataAtIndex(i);
                testDataPattern[countTest] = dataHandler.getPatternAtIndex(i);
                countTest += 1;
            }
        }
    }

    private void setDimensions(int splitCount) {
        trainingData = new double[dataHandler.getDataCount() - splitCount][dataHandler.getDataLength()];
        trainingDataPattern = new int[trainingData.length];
        testData = new double[splitCount][dataHandler.getDataLength()];
        testDataPattern = new int[testData.length];
    }
}
