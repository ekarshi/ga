package example.ann.data;

import java.util.Arrays;

/**
 * Normalize the given data in Range(0,1)
 * using MinMax normalization
 * (val - min)/(min - max)
 */
class NormalizeData {

    private NormalizeData() {
    }

    public static void normalize(double[][] data) {
        for (int i = 0; i < data[0].length; i++) {
            double[] column = extractColumn(i, data);
            double min = Arrays.stream(column).min().getAsDouble();
            double max = Arrays.stream(column).max().getAsDouble();
            setColumn(i, minMaxNormalize(min, max, column), data);
        }
    }

    private static double[] minMaxNormalize(double min, double max, double[] data) {
        return Arrays.stream(data).map(x -> (x - min) / (max - min)).toArray();
    }

    private static double[] extractColumn(int col, double[][] data) {
        double[] record = new double[data.length];
        for (int i = 0; i < data.length; i++)
            record[i] = data[i][col];
        return record;
    }

    private static void setColumn(int col, double[] values, double[][] data) {
        for (int i = 0; i < data.length; i++)
            data[i][col] = values[i];
    }
}
