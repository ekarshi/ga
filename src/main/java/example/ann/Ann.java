package example.ann;

import example.ann.data.DataHandler;
import example.ann.data.DataSet;
import example.ann.neuralnet.NetOptimizer;
import example.ann.neuralnet.NeuralNet;
import example.ann.util.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;


/**
 * Simple program to show case the use of
 * GA for initial weight tuning of Neural Net.
 * Neural Networks weight varies due to Randomization of the initial weights
 * How well a net is trained is dependent on the initial weights used
 * GA can be used to establish the initial weight or in rather simple cases
 * can establish result close to the Back Propagation Algorithm.
 * However better results might be achieved if GA could be used to initialize the
 * weights of the neural net to approx 80 - 90 % accuracy and then trained further
 * by the Back propagation learning algorithm.
 * <p>
 * This program only show cases a simple use of GA to train a simple Feed Forward Neural Net
 */
class Ann {
    private static final Logger LOGGER = LogManager.getLogger(Ann.class);

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //Set up the data handler to read the contents of the file and normalize the data
        DataHandler dataHandler = new DataHandler("benchmark_wine.csv", 14, 1);

        //Create a in memory data set from the data handler consisting of training and test set
        DataSet dataSet = new DataSet(dataHandler, 0.3);

        //Use the data set with the net optimizer to create a neural net of the given config
        NetOptimizer netOptimizer = new NetOptimizer(dataSet, 13, 3);
        NeuralNet neuralNet = netOptimizer.generateNetwork();

        //Test the network we have the test set inside data set
        testNetwork(neuralNet, dataSet);
    }

    private static void testNetwork(NeuralNet neuralNet, DataSet dataSet) {
        LOGGER.info("Samples in test data set {}", dataSet.testRecordCount());
        for (int i = 0; i < dataSet.testRecordCount(); i++) {
            int prediction = Utility.maxIndex(neuralNet.netOut(dataSet.getTestRecord(i)));
            boolean matched = dataSet.matchTestRecord(i, prediction);
            if (!matched)
                LOGGER.info("Matched  = {}, given sample = {}, predicted sample = {}", dataSet.matchTestRecord(i, prediction), dataSet.getPattern(dataSet.getTestPatternAtIndex(i)), dataSet.getPattern(prediction));
        }
    }
}
