package example.ann.util;

public class Utility {
    public static int maxIndex(double[] arr) {
        double max = arr[0];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                index = i;
                max = arr[i];
            }
        }
        return index;
    }
}
