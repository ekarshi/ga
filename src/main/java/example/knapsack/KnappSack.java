package example.knapsack;

import example.knapsack.data.KnappSackData;
import ga.algorithm.AbstractGA;
import ga.algorithm.AlgorithmConfig;
import ga.algorithm.IncrementalGA;
import ga.constants.CrossOverAlgorithms;
import ga.constants.MutationAlgorithms;
import ga.constants.SelectionAlgorithms;
import ga.fitness.Fitness;
import ga.model.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

class KnappSack {
    private static final Logger LOGGER = LogManager.getLogger(KnappSack.class);

    private static final Fitness<Chromosome<Byte>, Byte> fitness = chromosome -> {
        int accumulatedValue = 0;
        double accumulatedPrice = 0;
        for (int i = 0; i < chromosome.getGeneData().length; i++) {
            if (chromosome.getGeneAtLocation(i).byteValue() == (byte) 1) {
                accumulatedValue += KnappSackData.productInfo.get(i).getValue();
                accumulatedPrice += KnappSackData.productInfo.get(i).getPrice();
            }
        }
        chromosome.setFitnessValue(accumulatedValue - 36 * Math.abs(accumulatedPrice - KnappSackData.maxPrice));
    };

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Map<Byte, Byte> config = new HashMap<>();
        config.put((byte) 1, (byte) 0);
        config.put((byte) 0, (byte) 1);
        AlgorithmConfig<Chromosome<Byte>, Byte> algorithmConfig = new AlgorithmConfig.Builder<Chromosome<Byte>, Byte>()
                .initialChromosomes(KnappSackData::randomSolutions)
                .fitnessFunction(fitness)
                .selectionAlgorithm(SelectionAlgorithms.TOURNAMENT_SELECTION)
                .crossOverAlgorithm(CrossOverAlgorithms.SINGLE_POINT_CROSSOVER)
                .mutationAlgorithm(MutationAlgorithms.FLIP_MUTATION, config)
                .maxGenerationCount(45)
                .build();
        AbstractGA<Chromosome<Byte>, Byte> ga = new IncrementalGA<>(algorithmConfig);
        ga.generate();
        logSolution(ga);
    }

    private static void logSolution(AbstractGA<Chromosome<Byte>, Byte> abstractGA) {
        Chromosome<Byte> chromosome = abstractGA.solution();
        StringBuilder out = new StringBuilder();
        for (Byte b : chromosome.getGeneData()) {
            out.append(b.byteValue()).append(" ");
        }
        LOGGER.info("Solution {} ", out.toString());
    }
}
