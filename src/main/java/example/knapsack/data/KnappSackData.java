package example.knapsack.data;

import ga.model.Chromosome;

import java.util.*;

public class KnappSackData {
    public static final Map<Integer, Product> productInfo = new HashMap<>();
    public static final double maxPrice = 9.0;

    static {
        productInfo.put(0, new Product(6, 2));
        productInfo.put(1, new Product(5, 3));
        productInfo.put(2, new Product(8, 6));
        productInfo.put(3, new Product(9, 7));
        productInfo.put(4, new Product(6, 5));
        productInfo.put(5, new Product(7, 9));
        productInfo.put(6, new Product(3, 4));
    }

    public static List<Chromosome<Byte>> randomSolutions() {
        Random random = new Random();
        List<Chromosome<Byte>> chromosomes = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            int randomNumber = random.nextInt((int) Math.pow(2, productInfo.size()));
            chromosomes.add(new Chromosome<>(intToByte(randomNumber, productInfo.size())));
        }
        return chromosomes;
    }

    private static Byte[] intToByte(int number, int size) {
        Byte[] bytes = new Byte[size];
        for (int i = 0; i < size; i++) {
            bytes[i] = (byte) 0;
        }
        int remainder;
        int j = 0;
        while (number > 0) {
            remainder = number % 2;
            bytes[j] = (byte) remainder;
            j += 1;
            number = number / 2;
        }
        return bytes;
    }
}
