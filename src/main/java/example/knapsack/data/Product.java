package example.knapsack.data;

public class Product {
    private final int value;
    private final double price;

    public Product(int value, double price) {
        this.value = value;
        this.price = price;
    }

    public int getValue() {
        return value;
    }

    public double getPrice() {
        return price;
    }
}
