package ga.factory;

import ga.constants.CrossOverAlgorithms;
import ga.constants.MutationAlgorithms;
import ga.model.Chromosome;
import ga.model.ChromosomePool;
import ga.operation.crossover.CrossOver;
import ga.operation.mutation.Mutation;
import ga.operation.selection.Selection;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;

/**
 * Factory for creating all the supported algorithms
 * With different parameter options
 */
public class GAOperationFactory {

    private GAOperationFactory() {
    }

    public static <T extends Chromosome<U>, U> Selection<T, U> createSelection(Class<T> clazz, Object... args) throws
            IllegalAccessException,
            InstantiationException,
            NoSuchMethodException,
            InvocationTargetException {
        return hasInterface(clazz, Selection.class) ? (Selection<T, U>) clazz.getDeclaredConstructor(ChromosomePool.class).newInstance(args) : null;
    }

    public static <T extends Chromosome<U>, U> CrossOver<T, U> createCrossOver(Class<T> clazz, Object... args) throws
            IllegalAccessException,
            InstantiationException,
            NoSuchMethodException,
            InvocationTargetException {

        if ((clazz == CrossOverAlgorithms.BLX_ALPHA_CROSSOVER.getClazz() || clazz == CrossOverAlgorithms.UNIFORM_CROSSOVER.getClazz()) && argsCheck(1, args))
            return (CrossOver<T, U>) clazz.getDeclaredConstructor(Double.class).newInstance(args);

        if (clazz == CrossOverAlgorithms.CONVEX_COMBINATION_CROSSOVER.getClazz() || clazz == CrossOverAlgorithms.SBX_CROSSOVER.getClazz()) {
            if (argsCheck(2, args))
                return (CrossOver<T, U>) clazz.getDeclaredConstructor(Number.class, Number.class).newInstance(args);
            if (argsCheck(3, args))
                return (CrossOver<T, U>) clazz.getDeclaredConstructor(Number.class, Number.class, Double.class).newInstance(args);
        }
        return hasInterface(clazz, CrossOver.class) ? (CrossOver<T, U>) clazz.newInstance() : null;
    }

    public static <T extends Chromosome<U>, U> Mutation<T, U> createMutation(Class<T> clazz, Object... args) throws
            IllegalAccessException,
            InstantiationException,
            NoSuchMethodException,
            InvocationTargetException {
        if (clazz == MutationAlgorithms.FLIP_MUTATION.getClazz())
            return (Mutation<T, U>) clazz.getDeclaredConstructor(Map.class).newInstance(args);

        if (clazz == MutationAlgorithms.MULTIPOINT_MUTATION.getClazz())
            return (Mutation<T, U>) clazz.getDeclaredConstructor(Map.class, Double.class).newInstance(args);

        if (clazz == MutationAlgorithms.GAUSSIAN_MUTATION.getClazz() || clazz == MutationAlgorithms.RANDOM_MUTATION.getClazz() && argsCheck(3, args))
            return (Mutation<T, U>) clazz.getDeclaredConstructor(Number.class, Number.class, Double.class).newInstance(args);

        return hasInterface(clazz, Mutation.class) ? (Mutation<T, U>) clazz.newInstance() : null;
    }

    private static boolean argsCheck(int count, Object... args) {
        return null != args && args.length == count;
    }

    private static boolean hasInterface(Class clazzA, Class clazzB) {
        return Arrays.stream(clazzA.getInterfaces()).filter(x -> x.equals(clazzB)).count() > 0;
    }
}
