package ga.model;

import ga.fitness.Fitness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Representation of a pool of chromosomes
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */
public class ChromosomePool<T extends Chromosome<U>, U> {
    private static final Logger LOGGER = LogManager.getLogger(ChromosomePool.class);
    private final List<T> pool;
    private final Fitness<T, U> fitnessFunction;

    public ChromosomePool(List<T> chromosomes, Fitness<T, U> fitnessFunction) {
        this.pool = chromosomes;
        this.fitnessFunction = fitnessFunction;
        pool.forEach(fitnessFunction::eval);
    }

    public ChromosomePool(Fitness<T, U> fitnessFunction) {
        this(new ArrayList<>(), fitnessFunction);
    }

    public T getChromosomeFromPool(int location) {
        return pool.get(location);
    }

    public void addChromosomeToPool(T chromosome) {
        fitnessFunction.eval(chromosome);
        pool.add(chromosome);
    }

    public double generationAverageFitness() {
        return poolFitness() / pool.size();
    }

    public double poolFitness() {
        return pool.stream().mapToDouble(Chromosome::getFitnessValue).sum();
    }

    /**
     * Calculate the fitness score of the chromosome
     * The fitness value is now available in the chromosome
     * Use chromosome stored fitness value to replace the chromosome
     *
     * @param chromosome
     * @return
     */
    public boolean replaceChromosomeFromPool(T chromosome) {
        fitnessFunction.eval(chromosome);
        pool.sort(Comparator.comparingDouble(Chromosome::getFitnessValue));
        int indexToReplace = IntStream.
                range(0, getPoolSize()).filter(index -> pool.get(index).getFitnessValue() < chromosome.getFitnessValue() && !isChromosomePresent(chromosome)).
                findFirst().orElse(-1);
        return replaceFromPool(indexToReplace, chromosome);
    }

    public int getPoolSize() {
        return pool.size();
    }

    public List<T> getPool() {
        return pool;
    }

    public Fitness<T, U> getFitnessFunction() {
        return fitnessFunction;
    }

    private boolean isChromosomePresent(T chromosome) {
        return pool.stream().filter(x -> x.equals(chromosome)).count() > 0;
    }

    private boolean replaceFromPool(int index, T chromosome) {
        if (index > -1) {
            pool.remove(index);
            pool.add(index, chromosome);
            return true;
        }
        return false;
    }
}
