package ga.model;

import org.apache.commons.lang3.ObjectUtils;

import java.util.Arrays;

/**
 * Representation of the chromosome
 * The basic building block of GA
 *
 * @param <T> The type of chromosome
 */
public class Chromosome<T> {
    private final T[] geneData;
    private double fitnessValue = 0.0;

    public Chromosome(T[] geneData) {
        this.geneData = geneData;
    }

    public Chromosome(Chromosome<T> otherChromosome) {
        geneData = ObjectUtils.clone(otherChromosome.geneData);
        fitnessValue = otherChromosome.fitnessValue;
    }

    public void setGeneAtLocation(int location, T gene) {
        geneData[location] = gene;
    }

    public T getGeneAtLocation(int location) {
        return geneData[location];
    }

    public int getGeneLength() {
        return geneData.length;
    }

    public T[] getGeneData() {
        return geneData;
    }

    public double getFitnessValue() {
        return fitnessValue;
    }

    public void setFitnessValue(double fitnessValue) {
        this.fitnessValue = fitnessValue;
    }

    public Chromosome<T> cloneGene() {
        return new Chromosome<>(ObjectUtils.clone(geneData));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Arrays.stream(geneData).forEach(i -> builder.append(" " + i + " "));
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chromosome<?> that = (Chromosome<?>) o;
        return Arrays.equals(geneData, that.geneData);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(geneData);
    }
}
