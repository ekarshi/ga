package ga.constants;

import ga.operation.crossover.nonordered.MultiPointCrossOver;
import ga.operation.crossover.nonordered.SinglePointCrossOver;
import ga.operation.crossover.nonordered.UniformCrossOver;
import ga.operation.crossover.ordered.CycleCrossover;
import ga.operation.crossover.ordered.OrderOneCrossover;
import ga.operation.crossover.real.BlxAlpha;
import ga.operation.crossover.real.ConvexCombinationNADX;
import ga.operation.crossover.real.Sbx;

/**
 * Enum class ga.constants representing all the
 * possible cross over ga.algorithm supplied by the api
 */
public enum CrossOverAlgorithms {

    MULTI_POINT_CROSSOVER(MultiPointCrossOver.class),
    SINGLE_POINT_CROSSOVER(SinglePointCrossOver.class),
    UNIFORM_CROSSOVER(UniformCrossOver.class),
    ORDER_ONE_CROSSOVER(OrderOneCrossover.class),
    CYCLE_CROSSOVER(CycleCrossover.class),
    BLX_ALPHA_CROSSOVER(BlxAlpha.class),
    CONVEX_COMBINATION_CROSSOVER(ConvexCombinationNADX.class),
    SBX_CROSSOVER(Sbx.class);

    private final Class clazz;

    CrossOverAlgorithms(Class clazz) {
        this.clazz = clazz;
    }

    public Class getClazz() {
        return clazz;
    }
}
