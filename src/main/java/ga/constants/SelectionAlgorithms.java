package ga.constants;

import ga.operation.selection.impl.RouletteWheel;
import ga.operation.selection.impl.StochasticUniversalSampling;
import ga.operation.selection.impl.TournamentSelection;

/**
 * Enum class ga.constants representing all the possible selection ga.algorithm supplied by the api
 */
public enum SelectionAlgorithms {

    ROULETTE_WHEEL_SELECTION(RouletteWheel.class),
    TOURNAMENT_SELECTION(TournamentSelection.class),
    STOCHASTIC_UNIVERSAL_SAMPLING(StochasticUniversalSampling.class);

    private final Class clazz;

    SelectionAlgorithms(Class clazz) {
        this.clazz = clazz;
    }

    public Class getClazz() {
        return clazz;
    }
}
