package ga.constants;

import ga.operation.mutation.binary.FlipMutation;
import ga.operation.mutation.binary.MultiPointMutation;
import ga.operation.mutation.ordered.InverseMutation;
import ga.operation.mutation.ordered.ScrambleMutation;
import ga.operation.mutation.ordered.SwapMutation;
import ga.operation.mutation.real.GaussianMutation;
import ga.operation.mutation.real.RandomMutation;

/**
 * Enum class ga.constants representing all the possible mutation ga.algorithm supplied by the api
 */
public enum MutationAlgorithms {

    INVERSE_MUTATION(InverseMutation.class),
    MULTIPOINT_MUTATION(MultiPointMutation.class),
    SCRAMBLE_MUTATION(ScrambleMutation.class),
    FLIP_MUTATION(FlipMutation.class),
    SWAP_MUTATION(SwapMutation.class),
    RANDOM_MUTATION(RandomMutation.class),
    GAUSSIAN_MUTATION(GaussianMutation.class);

    private final Class clazz;

    MutationAlgorithms(Class clazz) {
        this.clazz = clazz;
    }

    public Class getClazz() {
        return clazz;
    }
}
