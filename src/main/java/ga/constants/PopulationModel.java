package ga.constants;

/**
 * Enum class representing the population models for GA
 */
public enum PopulationModel {

    STEADY_STATE_GA("Steady state Genetic Algorithm"),
    INCREMENTAL_GA("Incremental Genetic Algorithm");

    private final String description;

    PopulationModel(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
