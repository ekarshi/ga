package ga.algorithm;

import ga.model.Chromosome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;

/**
 * In incremental GA replace the existing chromosome from the pool
 *
 * @param <T> The Chromosome
 * @param <U> The type of Chromosome
 */
public class IncrementalGA<T extends Chromosome<U>, U> extends AbstractGA<T, U> {

    private static final Logger LOGGER = LogManager.getLogger(IncrementalGA.class);

    public IncrementalGA(AlgorithmConfig<T, U> algorithmConfig) {
        super(algorithmConfig);
    }

    private boolean poolHasSolution() {
        if (null == solutionFinder)
            return maxGeneration == generationCount;
        return chromosomePool.getPool().stream().filter(solutionFinder).count() > 0;
    }


    @Override
    public void generate() {
        int childCount = (int) (chromosomePool.getPoolSize() * selectionRate);
        while (!poolHasSolution()) {
            List<T> childChromosomes = generateOffSprings(selectChromosome(), childCount);
            if (random.nextDouble() < mutationProbability)
                mutateChromosomes(childChromosomes);
            childChromosomes.forEach(chromosomePool::replaceChromosomeFromPool);
            generationCount += 1;
            LOGGER.info("Generation average fitness {}, generation count {}", chromosomePool.generationAverageFitness(), generationCount);
        }
        LOGGER.info("Solution Reached ");
    }

    @Override
    public T solution() {
        chromosomePool.getPool().sort(Comparator.comparingDouble(x -> x.getFitnessValue()));
        LOGGER.info("Fitness of best chromosome {}", chromosomePool.getChromosomeFromPool(chromosomePool.getPoolSize() - 1).getFitnessValue());
        return chromosomePool.getChromosomeFromPool(chromosomePool.getPoolSize() - 1);
    }
}
