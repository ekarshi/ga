package ga.algorithm;

import ga.fitness.Fitness;
import ga.model.Chromosome;
import ga.model.ChromosomePool;
import ga.operation.crossover.CrossOver;
import ga.operation.mutation.Mutation;
import ga.operation.selection.Selection;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;


/**
 * Generalized GA with common operations applicable
 * over Incremental GA and SteadyState GA
 *
 * @param <T> The Chromosome
 * @param <U> The type of Chromosome
 */
public abstract class AbstractGA<T extends Chromosome<U>, U> {
    private final CrossOver<T, U> crossOver;
    private final Mutation<T, U> mutation;
    private final Selection<T, U> selection;
    protected final ChromosomePool<T, U> chromosomePool;
    protected final Random random;
    protected final double selectionRate;
    protected final double mutationProbability;
    protected final Predicate<T> solutionFinder;
    protected long generationCount = 0;
    protected final long maxGeneration;
    private final double mutationRate;

    public AbstractGA(AlgorithmConfig<T, U> algorithmConfig) {
        this.crossOver = algorithmConfig.getCrossOver();
        this.mutation = algorithmConfig.getMutation();
        this.selection = algorithmConfig.getSelection();
        this.chromosomePool = algorithmConfig.getChromosomePool();
        this.selectionRate = algorithmConfig.getSelectionRate();
        this.mutationProbability = algorithmConfig.getMutationProbability();
        this.mutationRate = algorithmConfig.getMutationRate();
        this.solutionFinder = algorithmConfig.getSolutionFinder();
        this.maxGeneration = algorithmConfig.getMaxGenCount();
        random = new Random();
    }

    /**
     * Subject the off-springs to mutation
     *
     * @param chromosomes The generation of child chromosomes subjected to mutation
     */
    protected void mutateChromosomes(List<T> chromosomes) {
        int mutatedChromosomeCount = (int) (chromosomes.size() * mutationRate);
        random.ints(0, chromosomes.size()).limit(mutatedChromosomeCount).forEach(i -> chromosomes.set(i, mutation.mutate(chromosomes.get(i))));
    }

    /**
     * Use the selection ga.algorithm to get the selected chromosomes
     *
     * @return List of chromosomes selected from the pool
     */
    protected List<T> selectChromosome() {
        int selectionCount = (int) (chromosomePool.getPoolSize() * selectionRate);
        return selection.select(selectionCount);
    }

    /**
     * Create child chromosomes from the selected chromosomes
     * Usually two parents are selected and two children are produced
     * There are exceptions to this rule in GA, but limited to two child in this API
     *
     * @param selectedChromosomes parent chromosomes for reproduction
     * @param count               no of children to produce
     * @return newly created chromosomes
     */
    protected List<T> generateOffSprings(List<T> selectedChromosomes, int count) {
        List<T> childChromosomes = new ArrayList<>();
        random.ints(0, selectedChromosomes.size()).limit(count % 2 == 0 ? count : count + 1).forEach(i -> {
            int otherIndex = random.nextInt(selectedChromosomes.size());
            while (otherIndex == i)
                otherIndex = random.nextInt(selectedChromosomes.size());
            List<T> crossed = crossOver.cross(selectedChromosomes.get(i), selectedChromosomes.get(otherIndex));
            childChromosomes.add(crossed.get(0));
            childChromosomes.add(crossed.get(1));
        });
        if (childChromosomes.size() > count)
            childChromosomes.remove(random.nextInt(childChromosomes.size()));
        return childChromosomes;
    }

    public abstract void generate();

    public abstract T solution();
}
