package ga.algorithm;

import ga.constants.CrossOverAlgorithms;
import ga.constants.MutationAlgorithms;
import ga.constants.SelectionAlgorithms;
import ga.factory.GAOperationFactory;
import ga.fitness.Fitness;
import ga.model.Chromosome;
import ga.model.ChromosomePool;
import ga.operation.crossover.CrossOver;
import ga.operation.mutation.Mutation;
import ga.operation.selection.Selection;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;


/**
 * Build the algorithm with the required algorithms
 * and default parameter rates
 *
 * @param <T> The Chromosome
 * @param <U> The type of Chromosome
 */
public class AlgorithmConfig<T extends Chromosome<U>, U> {
    private Fitness<T, U> fitness;
    private CrossOver<T, U> crossOver;
    private Mutation<T, U> mutation;
    private Selection<T, U> selection;
    private ChromosomePool<T, U> chromosomePool;
    private Predicate<T> solutionFinder;
    private int maxGenCount = 0;

    //The no parents to select
    private double selectionRate = 0.5;

    //The generation affected by mutation
    private double mutationProbability = 0.2;

    //The no of children affected by mutation
    private double mutationRate = 0.2;

    public Fitness<T, U> getFitness() {
        return fitness;
    }

    public CrossOver<T, U> getCrossOver() {
        return crossOver;
    }

    public Mutation<T, U> getMutation() {
        return mutation;
    }

    public Selection<T, U> getSelection() {
        return selection;
    }

    public ChromosomePool<T, U> getChromosomePool() {
        return chromosomePool;
    }

    public double getSelectionRate() {
        return selectionRate;
    }

    public double getMutationProbability() {
        return mutationProbability;
    }

    public double getMutationRate() {
        return mutationRate;
    }

    public Predicate<T> getSolutionFinder() {
        return solutionFinder;
    }

    public int getMaxGenCount() {
        return maxGenCount;
    }

    public static class Builder<T extends Chromosome<U>, U> {
        private CrossOver<T, U> crossOver;
        private Mutation<T, U> mutation;
        private Selection<T, U> selection;
        private ChromosomePool<T, U> chromosomePool;
        private Fitness<T, U> fitnessFunction;
        private double selectionRate = 0.5;
        private double mutationProbability = 0.2;
        private double mutationRate = 0.2;
        private Predicate<T> solutionFinder;
        private SelectionAlgorithms selectionAlgorithms;
        private int maxGenCount = 0;
        private Supplier<List<T>> initialChromosomes;

        public Builder<T, U> fitnessFunction(Fitness<T, U> fitnessFunction) {
            this.fitnessFunction = fitnessFunction;
            return this;
        }

        public Builder<T, U> initialChromosomes(Supplier<List<T>> initialChromosomes) {
            this.initialChromosomes = initialChromosomes;
            return this;
        }

        public Builder<T, U> crossOverAlgorithm(CrossOverAlgorithms crossOverAlgorithms, Object... args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
            crossOver = GAOperationFactory.createCrossOver(crossOverAlgorithms.getClazz(), args);
            return this;
        }

        public Builder<T, U> selectionAlgorithm(SelectionAlgorithms selectionAlgorithms) {
            this.selectionAlgorithms = selectionAlgorithms;
            return this;
        }

        public Builder<T, U> mutationAlgorithm(MutationAlgorithms mutationAlgorithms, Object... args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            mutation = GAOperationFactory.createMutation(mutationAlgorithms.getClazz(), args);
            return this;
        }

        public Builder<T, U> selectionRate(double rate) {
            selectionRate = rate;
            return this;
        }

        public Builder<T, U> mutationProbability(double probabilityForMutation) {
            mutationProbability = probabilityForMutation;
            return this;
        }

        public Builder<T, U> mutationRate(double rateOfMutation) {
            mutationRate = rateOfMutation;
            return this;
        }

        public Builder<T, U> solution(Predicate<T> solutionFinder) {
            this.solutionFinder = solutionFinder;
            return this;
        }

        public Builder<T, U> maxGenerationCount(int count) {
            this.maxGenCount = count;
            return this;
        }

        public AlgorithmConfig<T, U> build() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            this.chromosomePool = new ChromosomePool<>(this.initialChromosomes.get(), this.fitnessFunction);
            this.selection = GAOperationFactory.createSelection(selectionAlgorithms.getClazz(), chromosomePool);
            AlgorithmConfig<T, U> algorithmConfig = new AlgorithmConfig<>();
            algorithmConfig.fitness = this.fitnessFunction;
            algorithmConfig.crossOver = this.crossOver;
            algorithmConfig.mutation = this.mutation;
            algorithmConfig.selection = this.selection;
            algorithmConfig.selectionRate = this.selectionRate;
            algorithmConfig.chromosomePool = this.chromosomePool;
            algorithmConfig.mutationProbability = this.mutationProbability;
            algorithmConfig.mutationRate = this.mutationRate;
            algorithmConfig.solutionFinder = this.solutionFinder;
            algorithmConfig.maxGenCount = this.maxGenCount;

            return algorithmConfig;
        }
    }
}
