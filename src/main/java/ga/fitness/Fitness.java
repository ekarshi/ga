package ga.fitness;


import ga.model.Chromosome;

@FunctionalInterface
public interface Fitness<T extends Chromosome<U>, U> {
    void eval(T chromosome);
}
