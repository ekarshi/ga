package ga.operation.selection.impl;


import ga.model.Chromosome;
import ga.model.ChromosomePool;
import ga.operation.selection.Selection;

import java.util.*;

/**
 * Standard implementation of Stochastic Universal Sampling
 * No of chromosomes to be selected from the pool must be appropriately
 * set by the caller
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */
public class StochasticUniversalSampling<T extends Chromosome<U>, U> implements Selection {

    private final ChromosomePool<T, U> chromosomePool;

    public StochasticUniversalSampling(ChromosomePool<T, U> pool) {
        this.chromosomePool = pool;
    }

    @Override
    public List<T> select(int selectionCount) {
        double cumulativeSum = 0.0;
        Set<T> selectedSet = new LinkedHashSet<>();
        double[] pointers = setUpPointerDistance(selectionCount);

        for (int i = 0; i < pointers.length; i++) {
            for (int j = 0; j < chromosomePool.getPoolSize(); j++) {
                cumulativeSum = cumulativeSum + chromosomePool.getChromosomeFromPool(i).getFitnessValue();
                if (cumulativeSum > pointers[i]) {
                    selectedSet.add(chromosomePool.getChromosomeFromPool(j));
                    cumulativeSum = 0.0;
                    break;
                }
            }
        }
        return new ArrayList<>(selectedSet);
    }

    private double[] setUpPointerDistance(int selectionCount) {
        Random random = new Random();
        double totalFitness = chromosomePool.poolFitness();
        double pointerDistance = totalFitness / selectionCount;
        double[] randomNumber = random.doubles(1, 0.0, pointerDistance).toArray();
        double[] pointers = new double[selectionCount];

        for (int i = 0; i < selectionCount; i++)
            pointers[i] = randomNumber[0] + i * pointerDistance;

        return pointers;
    }
}
