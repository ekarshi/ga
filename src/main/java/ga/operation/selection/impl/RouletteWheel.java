package ga.operation.selection.impl;


import ga.model.Chromosome;
import ga.model.ChromosomePool;
import ga.operation.selection.Selection;
import org.apache.commons.lang.math.RandomUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of the standard roulette wheel selection
 * No of elements to be selected from the chromosome pool must appropriately
 * set by the caller.
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */
public class RouletteWheel<T extends Chromosome<U>, U> implements Selection {

    private final ChromosomePool<T, U> pool;
    private double[] selectionProbability;

    public RouletteWheel(ChromosomePool<T, U> pool) {
        this.pool = pool;
    }

    @Override
    public List<T> select(int selectionCount) {
        Set<T> selectedSet = new LinkedHashSet<>();
        calculateSelectionProbabilities();
        while (selectedSet.size() != selectionCount) {
            double randomNumber = RandomUtils.nextDouble();
            for (int j = 0; j < selectionProbability.length; j++) {
                if (randomNumber < selectionProbability[j]) {
                    selectedSet.add(pool.getChromosomeFromPool(j));
                    break;
                }
            }
        }
        return new ArrayList<>(selectedSet);
    }

    private void calculateSelectionProbabilities() {
        selectionProbability = new double[pool.getPoolSize()];
        double poolFitness = pool.poolFitness();
        double prevProbability = 0.0;
        for (int i = 0; i < selectionProbability.length; i++) {
            selectionProbability[i] = prevProbability + pool.getChromosomeFromPool(i).getFitnessValue() / poolFitness;
            prevProbability = selectionProbability[i];
        }
    }
}
