package ga.operation.selection.impl;

import ga.model.Chromosome;
import ga.model.ChromosomePool;
import ga.operation.selection.Selection;

import java.util.*;

public class TournamentSelection<T extends Chromosome<U>, U> implements Selection<T, U> {

    private final Set<T> selectedSet;
    private final ChromosomePool<T, U> chromosomePool;
    private final int kWay = 3;
    private final Random random;


    public TournamentSelection(ChromosomePool<T, U> chromosomePool) {
        selectedSet = new HashSet<>();
        this.chromosomePool = chromosomePool;
        random = new Random();
    }

    @Override
    public List<T> select(int selectionCount) {
        while (!(selectedSet.size() == selectionCount)) {
            ArrayList<T> tournamentSelect = new ArrayList<>();
            for (int i = 0; i < kWay; i++)
                tournamentSelect.add(chromosomePool.getChromosomeFromPool(random.nextInt(chromosomePool.getPoolSize())));
            tournamentSelect.sort((a, b) -> (int) (a.getFitnessValue() - b.getFitnessValue()));
            selectedSet.add(tournamentSelect.get(tournamentSelect.size() - 1));
        }
        return new ArrayList<>(selectedSet);
    }
}
