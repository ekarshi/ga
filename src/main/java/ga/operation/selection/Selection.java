package ga.operation.selection;

import ga.model.Chromosome;

import java.util.List;

public interface Selection<T extends Chromosome<U>, U> {

    List<T> select(int selectionCount);

}
