package ga.operation.crossover.real;

import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import ga.utility.GeneUtility;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.util.List;

/**
 * Convex combination cross-over uses a random number (0,1)
 * While NADX uses the normal distribution with mean at 0 and std deviation at 1
 * <p>
 * This implements using the normal distribution function
 *
 * @param <T>
 * @param <U>
 */
public class ConvexCombinationNADX<T extends Chromosome<U>, U extends Number> implements CrossOver<T, U> {

    private final U upperBound;
    private final U lowerBound;
    private double crossOverRate = 0.5;
    private final RandomDataGenerator randomDataGenerator;

    public ConvexCombinationNADX(U lowerBound, U upperBound) {
        this.upperBound = upperBound;
        this.lowerBound = lowerBound;
        this.randomDataGenerator = new RandomDataGenerator();
    }

    public ConvexCombinationNADX(U lowerBound, U upperBound, Double crossOverRate) {
        this(lowerBound, upperBound);
        this.crossOverRate = crossOverRate;
    }

    @Override
    public List<T> cross(T... chromosomes) {
        T cloneA = (T) chromosomes[0].cloneGene();
        T cloneB = (T) chromosomes[1].cloneGene();
        for (int loc = 0; loc < cloneA.getGeneLength(); loc++) {
            if (RandomUtils.nextDouble() < crossOverRate) {
                double lambda = randomDataGenerator.nextGaussian(0, 1);
                double pGeneA = cloneA.getGeneAtLocation(loc).doubleValue();
                double pGeneB = cloneB.getGeneAtLocation(loc).doubleValue();
                U geneA = GeneUtility.clampValues(lowerBound, upperBound, (U) (Double) (lambda * pGeneA + (1 - lambda) * pGeneB));
                U geneB = GeneUtility.clampValues(lowerBound, upperBound, (U) (Double) ((1 - lambda) * pGeneA + lambda * pGeneB));
                cloneA.setGeneAtLocation(loc, geneA);
                cloneB.setGeneAtLocation(loc, geneB);
            }
        }
        return createChildGeneContainer(cloneA, cloneB);
    }
}
