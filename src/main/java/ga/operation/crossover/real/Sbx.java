package ga.operation.crossover.real;

import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import ga.utility.GeneUtility;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.math3.analysis.function.Pow;

import java.util.List;

/**
 * @author myname
 */
public class Sbx<T extends Chromosome<U>, U extends Number> implements CrossOver<T, U> {
    private double eta = 2;
    private final U lowerBound;
    private final U upperBound;
    private final Pow pow;

    public Sbx(U lowerBound, U upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        pow = new Pow();
    }

    public Sbx(U lowerBound, U upperBound, Double eta) {
        this(lowerBound, upperBound);
        this.eta = eta;
    }

    @Override
    public List<T> cross(T... chromosomes) {
        T cloneA = (T) chromosomes[0].cloneGene();
        T cloneB = (T) chromosomes[1].cloneGene();
        for (int i = 0; i < cloneA.getGeneLength(); i++) {
            double beta = ordinate();
            U geneA = (U) (Double) (0.5 * ((1 + beta) * cloneA.getGeneAtLocation(i).doubleValue() + (1 - beta) * cloneB.getGeneAtLocation(i).doubleValue()));
            U geneB = (U) (Double) (0.5 * ((1 - beta) * cloneA.getGeneAtLocation(i).doubleValue() + (1 + beta) * cloneB.getGeneAtLocation(i).doubleValue()));
            cloneA.setGeneAtLocation(i, GeneUtility.clampValues(lowerBound, upperBound, geneA));
            cloneB.setGeneAtLocation(i, GeneUtility.clampValues(lowerBound, upperBound, geneB));

        }
        return createChildGeneContainer(cloneA, cloneB);
    }

    private double ordinate() {
        double u = RandomUtils.nextDouble();
        return u < 0.5 ? pow.value(2 * u, 1 / (eta + 1)) : pow.value(1 / (2 * (1 - u)), 1 / (eta + 1));
    }
}
