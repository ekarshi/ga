package ga.operation.crossover.real;

import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.util.List;

public class BlxAlpha<T extends Chromosome<U>, U extends Number> implements CrossOver<T, U> {
    private double alpha = 0.5;
    private final RandomDataGenerator randomDataGenerator;

    public BlxAlpha() {
        this.randomDataGenerator = new RandomDataGenerator();
    }

    public BlxAlpha(double alpha) {
        this();
        this.alpha = alpha;
    }

    @Override
    public List<T> cross(T... chromosomes) {
        T cloneA = (T) chromosomes[0].cloneGene();
        T cloneB = (T) chromosomes[1].cloneGene();
        for (int loc = 0; loc < cloneA.getGeneLength(); loc++) {
            cloneA.setGeneAtLocation(loc, (U) crossedGeneValue(cloneA.getGeneAtLocation(loc).doubleValue(), cloneB.getGeneAtLocation(loc).doubleValue()));
            cloneB.setGeneAtLocation(loc, (U) crossedGeneValue(cloneB.getGeneAtLocation(loc).doubleValue(), cloneA.getGeneAtLocation(loc).doubleValue()));
        }
        return createChildGeneContainer(cloneA, cloneB);
    }

    private Double crossedGeneValue(double geneA, double geneB) {
        if (geneA == geneB)
            return geneA;
        double distance = Math.abs(geneA - geneB);
        double min = Math.min(geneA, geneB) - this.alpha * distance;
        double max = Math.max(geneA, geneB) + this.alpha * distance;
        return randomDataGenerator.nextUniform(min, max, false);
    }
}
