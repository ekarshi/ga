package ga.operation.crossover.ordered;

import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CycleCrossover<T extends Chromosome<U>, U> implements CrossOver<T, U> {
    private Set<Integer> selected;
    private T parentA;
    private T parentB;
    private T childA;
    private T childB;

    public CycleCrossover() {
        selected = new HashSet<>();
    }

    @Override
    public List<T> cross(T... chromosomes) {
        parentA = chromosomes[0];
        parentB = chromosomes[1];
        childA = (T) chromosomes[0].cloneGene();
        childB = (T) chromosomes[1].cloneGene();
        cycle();
        List<T> children = new LinkedList<>();
        children.add(childA);
        children.add(childB);
        selected.clear();
        return children;
    }

    private void cycle() {
        int i = 2;
        while (!(selected.size() == parentA.getGeneLength())) {
            int loc = getLocation();
            U checkGene = parentA.getGeneAtLocation(loc);
            selected.add(loc);
            updateChild(loc, i % 2 == 0);
            findGene(checkGene, parentB.getGeneAtLocation(loc), i % 2 == 0);
            i += 1;
        }
    }

    private int getLocation() {
        for (int i = 0; i < parentA.getGeneLength(); i++) {
            if (!selected.contains(i))
                return i;
        }
        return -1;
    }

    private void findGene(U checkGene, U gene, boolean seqUpdate) {
        if (gene.equals(checkGene))
            return;
        int loc = findInParent(gene, parentA);
        selected.add(loc);
        gene = parentB.getGeneAtLocation(loc);
        updateChild(loc, seqUpdate);
        findGene(checkGene, gene, seqUpdate);
    }

    private void updateChild(int loc, boolean turn) {
        if (turn) {
            childA.setGeneAtLocation(loc, parentA.getGeneAtLocation(loc));
            childB.setGeneAtLocation(loc, parentB.getGeneAtLocation(loc));
        } else {
            childB.setGeneAtLocation(loc, parentA.getGeneAtLocation(loc));
            childA.setGeneAtLocation(loc, parentB.getGeneAtLocation(loc));
        }
    }

    private int findInParent(U allele, T parent) {
        for (int i = 0; i < parent.getGeneLength(); i++) {
            if (parent.getGeneAtLocation(i).equals(allele))
                return i;
        }
        return -1;
    }
}
