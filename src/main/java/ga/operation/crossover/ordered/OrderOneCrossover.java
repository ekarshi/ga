package ga.operation.crossover.ordered;


import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import ga.utility.GeneUtility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OrderOneCrossover<T extends Chromosome<U>, U> implements CrossOver<T, U> {

    @Override
    public List<T> cross(T... chromosomes) {
        T childChromosomeA = (T) chromosomes[0].cloneGene();
        T childChromosomeB = (T) chromosomes[1].cloneGene();
        List<T> childChromosomes = new ArrayList<>();
        int crossSiteA = GeneUtility.generateLocation(chromosomes[0].getGeneLength());
        int crossSiteB = GeneUtility.generateLocation(chromosomes[1].getGeneLength(), crossSiteA);

        nullSetGene(childChromosomeA, childChromosomeB, 0, crossSiteA);
        nullSetGene(childChromosomeA, childChromosomeB, crossSiteB + 1, childChromosomeA.getGeneLength());

        fillGap(childChromosomeA, chromosomes[1]);
        fillGap(childChromosomeB, chromosomes[0]);

        childChromosomes.add(childChromosomeA);
        childChromosomes.add(childChromosomeB);

        return childChromosomes;
    }

    private void fillGap(T childChromosome, T secondParent) {
        Set<U> geneData = new HashSet<>();
        setUpUniqueGene(childChromosome, geneData);
        fillChildChromosomeFromParent(childChromosome, secondParent, geneData);
    }

    private void fillChildChromosomeFromParent(T childChromosome, T parent, Set<U> geneData) {
        int externalCounter = 0;
        for (int i = 0; i < childChromosome.getGeneLength(); i++) {
            if (null == childChromosome.getGeneAtLocation(i)) {
                for (int j = externalCounter; j < parent.getGeneLength(); j++) {
                    if (!geneData.contains(parent.getGeneAtLocation(j))) {
                        childChromosome.setGeneAtLocation(i, parent.getGeneAtLocation(j));
                        externalCounter = externalCounter + 1;
                        break;
                    }
                    externalCounter = externalCounter + 1;
                }
            }
        }
    }

    private void setUpUniqueGene(T childChromosome, Set<U> geneData) {
        for (int i = 0; i < childChromosome.getGeneLength(); i++) {
            if (null != childChromosome.getGeneAtLocation(i)) {
                geneData.add(childChromosome.getGeneAtLocation(i));
            }
        }
    }

    private void nullSetGene(T chromosomeA, T chromosomeB, int start, int end) {
        for (int i = start; i < end; i++) {
            chromosomeA.setGeneAtLocation(i, null);
            chromosomeB.setGeneAtLocation(i, null);
        }
    }
}
