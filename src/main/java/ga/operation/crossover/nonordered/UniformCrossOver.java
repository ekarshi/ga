package ga.operation.crossover.nonordered;


import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import org.apache.commons.lang.math.RandomUtils;

import java.util.List;

/**
 * For every gene if the random number is less than
 * standard probability, the genes at the location
 * is swapped between the parents
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */

public class UniformCrossOver<T extends Chromosome<U>, U> implements CrossOver<T, U> {
    private double stdCrossOverProbability = 0.5;

    public UniformCrossOver(double stdCrossOverProbability) {
        this.stdCrossOverProbability = stdCrossOverProbability;
    }

    public UniformCrossOver() {
    }

    @Override
    public List<T> cross(T... chromosomes) {
        T cloneA = (T) chromosomes[0].cloneGene();
        T cloneB = (T) chromosomes[1].cloneGene();
        for (int i = 0; i < cloneA.getGeneLength(); i++) {
            if (RandomUtils.nextDouble() < stdCrossOverProbability) {
                swapGene(cloneA, cloneB, i);
            }
        }
        return createChildGeneContainer(cloneA, cloneB);
    }
}
