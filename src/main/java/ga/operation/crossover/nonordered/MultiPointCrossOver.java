package ga.operation.crossover.nonordered;


import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import ga.utility.GeneUtility;

import java.util.List;

/**
 * Randomly select two locations
 * swap the genes in between
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 * @author Ekarshi Mitra
 */
public class MultiPointCrossOver<T extends Chromosome<U>, U> implements CrossOver<T, U> {
    @Override
    public List<T> cross(T... chromosomes) {
        int crossSiteA = GeneUtility.generateLocation(chromosomes[0].getGeneLength());
        int crossSiteB = GeneUtility.generateLocation(chromosomes[0].getGeneLength(), crossSiteA);
        return cross((T) chromosomes[0].cloneGene(), (T) chromosomes[1].cloneGene(), crossSiteA, crossSiteB);
    }
}
