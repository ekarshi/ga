package ga.operation.crossover.nonordered;


import ga.model.Chromosome;
import ga.operation.crossover.CrossOver;
import ga.utility.GeneUtility;

import java.util.List;

/**
 * Select a random point for crossover
 * swap the genes between the parents
 * produces two off-springs
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */
public class SinglePointCrossOver<T extends Chromosome<U>, U> implements CrossOver<T, U> {

    @Override
    public List<T> cross(T... chromosomes) {
        int crossSiteA = GeneUtility.generateLocation(chromosomes[0].getGeneLength());
        return cross((T) chromosomes[0].cloneGene(), (T) chromosomes[1].cloneGene(), crossSiteA, chromosomes[0].getGeneLength() - 1);
    }
}
