package ga.operation.crossover;

import ga.model.Chromosome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Interface for crossover operations
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */
public interface CrossOver<T extends Chromosome<U>, U> {

    default void swapGene(T chromosomeA, T chromosomeB, int location) {
        U temp = chromosomeA.getGeneAtLocation(location);
        chromosomeA.setGeneAtLocation(location, chromosomeB.getGeneAtLocation(location));
        chromosomeB.setGeneAtLocation(location, temp);
    }

    default List<T> createChildGeneContainer(T... children) {
        return new ArrayList<>(Arrays.asList(children));
    }

    default List<T> cross(T chromosomeA, T chromosomeB, int siteA, int siteB) {
        for (int i = siteA; i <= siteB; i++) {
            swapGene(chromosomeA, chromosomeB, i);
        }
        return createChildGeneContainer(chromosomeA, chromosomeB);
    }

    List<T> cross(T... chromosomes);
}
