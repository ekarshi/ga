package ga.operation.mutation;

import ga.model.Chromosome;

public interface Mutation<T extends Chromosome<U>, U> {

    default T swapGene(T chromosome, int locationA, int locationB) {
        U temp = chromosome.getGeneAtLocation(locationA);
        chromosome.setGeneAtLocation(locationA, chromosome.getGeneAtLocation(locationB));
        chromosome.setGeneAtLocation(locationB, temp);
        return chromosome;
    }

    T mutate(T chromosome);
}
