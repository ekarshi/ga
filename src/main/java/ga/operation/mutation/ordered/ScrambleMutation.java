package ga.operation.mutation.ordered;

import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import ga.utility.GeneUtility;
import org.apache.commons.lang.math.RandomUtils;

/**
 * Select two random points on the chromosome
 * Shuffle the genes in the chromosome between these two locations
 * <p>
 * Can be used with both binary and permutation based encoding
 *
 * @author Ekarshi Mitra
 */

public class ScrambleMutation<T extends Chromosome<U>, U> implements Mutation<T, U> {
    private int startLocation = 0;
    private int endLocation = 0;
    private T cloneChromosome;

    @Override
    public T mutate(T chromosome) {
        cloneChromosome = (T) chromosome.cloneGene();
        startLocation = GeneUtility.generateLocation(cloneChromosome.getGeneLength());
        endLocation = RandomUtils.nextInt(cloneChromosome.getGeneLength());
        if (startLocation > endLocation) {
            swapLocation();
        }
        shuffleAlleles();
        return cloneChromosome;
    }

    private void swapLocation() {
        startLocation = startLocation ^ endLocation;
        endLocation = startLocation ^ endLocation;
        startLocation = startLocation ^ endLocation;
    }

    private void shuffleAlleles() {
        int index;
        for (int i = startLocation; i <= endLocation; i++) {
            index = RandomUtils.nextInt(i + 1);
            swapGene(cloneChromosome, i, index);
        }
    }
}
