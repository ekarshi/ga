package ga.operation.mutation.ordered;

import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import ga.utility.GeneUtility;

/**
 * Select two random locations on the chromosome
 * Swap the genes at the random locations
 * <p>
 * Suitable for permutation based encoding
 *
 * @author Ekarshi Mitra
 */

public class SwapMutation<T extends Chromosome<U>, U> implements Mutation<T, U> {
    @Override
    public T mutate(T chromosome) {
        T clonedChromosome = (T) chromosome.cloneGene();
        int locationA = GeneUtility.generateLocation(clonedChromosome.getGeneLength());
        int locationB = GeneUtility.generateLocation(clonedChromosome.getGeneLength());
        return swapGene((T) chromosome.cloneGene(), locationA, locationB);
    }
}
