package ga.operation.mutation.ordered;

import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import org.apache.commons.lang.math.RandomUtils;

/**
 * Select two random location
 * reverse the alleles between the locations
 * Can be used with both binary encoding and permutation encoding
 *
 * @author Ekarshi Mitra
 */
public class InverseMutation<T extends Chromosome<U>, U> implements Mutation<T, U> {

    private int startLocation = 0;
    private int endLocation = 0;
    private T clonedChromosome;

    @Override
    public T mutate(T chromosome) {
        clonedChromosome = (T) chromosome.cloneGene();
        startLocation = RandomUtils.nextInt(clonedChromosome.getGeneLength());
        endLocation = RandomUtils.nextInt(clonedChromosome.getGeneLength());
        if (startLocation > endLocation)
            swapLocation();
        reverseAlleles();
        return clonedChromosome;
    }

    private void swapLocation() {
        if (startLocation > endLocation) {
            startLocation = startLocation ^ endLocation;
            endLocation = startLocation ^ endLocation;
            startLocation = startLocation ^ endLocation;
        }
    }

    private void reverseAlleles() {
        while (startLocation < endLocation) {
            swapGene(clonedChromosome, startLocation, endLocation);
            startLocation = startLocation + 1;
            endLocation = endLocation - 1;
        }
    }
}
