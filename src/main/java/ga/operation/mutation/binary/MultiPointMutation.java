package ga.operation.mutation.binary;


import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import ga.utility.GeneUtility;

import java.util.Map;

/**
 * Similar to FlipMutation but affecting multiple genes
 * determined by the mutation rate
 *
 * @param <T> Chromosome
 * @param <U> The type of chromosome
 */
public class MultiPointMutation<T extends Chromosome<U>, U> implements Mutation<T, U> {

    private final Map<U, U> config;
    private final double mutationRate;

    /**
     * @param config       Map determines the values the Gene should be altered to
     * @param mutationRate the number of genes in the chromosome to be affected
     */
    public MultiPointMutation(Map<U, U> config, double mutationRate) {
        this.config = config;
        this.mutationRate = mutationRate;
    }

    @Override
    public T mutate(T chromosome) {
        int affectedAllelesCount = (int) (chromosome.getGeneLength() * mutationRate);
        T clonedChromosome = (T) chromosome.cloneGene();
        for (int i = 0; i < affectedAllelesCount; i++) {
            int location = GeneUtility.generateLocation(clonedChromosome.getGeneLength());
            clonedChromosome.setGeneAtLocation(location, config.get(clonedChromosome.getGeneAtLocation(location)));
        }
        return clonedChromosome;
    }
}
