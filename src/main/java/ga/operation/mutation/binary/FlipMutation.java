package ga.operation.mutation.binary;

import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import org.apache.commons.lang.math.RandomUtils;

import java.util.Map;

/**
 * FlipMutation, flip the gene at random location of the chromosome
 * Genes to be flipped should be set using a Map
 * Each key of the map corresponding to the value, the gene should be altered to
 * <p>
 * Not suitable for permutation encoding
 *
 * @param <T> The chromosome
 * @param <U> The type of Chromosome
 * @ Author Ekarshi Mitra
 */
public class FlipMutation<T extends Chromosome<U>, U> implements Mutation<T, U> {
    private final Map<U, U> configMap;

    /**
     * @param config Map determines the genes should be altered to
     */
    public FlipMutation(Map<U, U> config) {
        configMap = config;
    }

    @Override
    public T mutate(T chromosome) {
        T cloneChromosome = (T) chromosome.cloneGene();
        int location = RandomUtils.nextInt(cloneChromosome.getGeneLength());
        cloneChromosome.setGeneAtLocation(location, configMap.get(cloneChromosome.getGeneAtLocation(location)));
        return cloneChromosome;
    }
}
