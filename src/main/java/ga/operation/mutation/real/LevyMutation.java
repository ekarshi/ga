package ga.operation.mutation.real;


import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.math3.distribution.LevyDistribution;
import org.apache.commons.math3.random.Well512a;

public class LevyMutation<T extends Chromosome<U>, U extends Number> implements Mutation<T, U> {

    private Double mutationRate = 0.5;
    private double mean = 0.0;
    private final LevyDistribution levyDistribution;

    public LevyMutation(U lowerBound, U upperBound, Double mutationRate) {
        this.mutationRate = mutationRate;
        this.mean = (upperBound.doubleValue() - lowerBound.doubleValue()) / 6;
        this.levyDistribution = new LevyDistribution(new Well512a(), mean, 1.0);
    }

    /**
     * https://en.wikipedia.org/wiki/Inverse_transform_sampling
     * https://en.wikipedia.org/wiki/Lévy_distribution
     *
     * @param chromosome
     * @return
     */
    @Override
    public T mutate(T chromosome) {
        T clonedChromosome = (T) chromosome.cloneGene();
        for (int i = 0; i < chromosome.getGeneLength(); i++) {
            if (RandomUtils.nextDouble() < mutationRate) {
                double u = RandomUtils.nextDouble();
                double inverse = levyDistribution.inverseCumulativeProbability(
                        levyDistribution.cumulativeProbability(clonedChromosome.getGeneAtLocation(i).doubleValue()));
                double value = u * inverse;
                clonedChromosome.setGeneAtLocation(i, (U) (Double) value);
            }
        }
        return clonedChromosome;
    }
}
