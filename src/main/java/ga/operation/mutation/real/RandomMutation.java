package ga.operation.mutation.real;

import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.math3.random.RandomDataGenerator;

public class RandomMutation<T extends Chromosome<U>, U extends Number> implements Mutation<T, U> {

    private final U upperBound;
    private final U lowerBound;
    private final double mutationRate;
    private final RandomDataGenerator randomDataGenerator;

    public RandomMutation(U upperBound, U lowerBound, double mutationRate) {
        this.upperBound = upperBound;
        this.lowerBound = lowerBound;
        this.mutationRate = mutationRate;
        this.randomDataGenerator = new RandomDataGenerator();
    }

    @Override
    public T mutate(T chromosome) {
        T clonedChromosome = (T) chromosome.cloneGene();
        for (int location = 0; location < clonedChromosome.getGeneLength(); location++) {
            if (RandomUtils.nextDouble() < mutationRate) {
                Double mutatedValue = randomDataGenerator.nextUniform(lowerBound.doubleValue(), upperBound.doubleValue(), true);
                clonedChromosome.setGeneAtLocation(location, (U) mutatedValue);
            }
        }
        return clonedChromosome;
    }
}
