package ga.operation.mutation.real;


import ga.model.Chromosome;
import ga.operation.mutation.Mutation;
import ga.utility.GeneUtility;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.math3.random.RandomDataGenerator;

/**
 * The mutated values is drawn from the gaussian distribution
 * with mean equal to the value of gene to be mutated
 * and std deviation 1/6th of the bounds of the gene
 *
 * @param <T> The chromosome
 * @param <U> The type of chromosome
 */
public class GaussianMutation<T extends Chromosome<U>, U extends Number> implements Mutation<T, U> {

    private final U upperBound;
    private final U loweBound;
    private final double mutationRate;
    private final double standardDeviation;
    private final RandomDataGenerator randomDataGenerator;

    public GaussianMutation(U lowerBound, U upperBound, Double mutationRate) {
        this.upperBound = upperBound;
        this.loweBound = lowerBound;
        this.mutationRate = mutationRate;
        this.standardDeviation = (upperBound.doubleValue() - loweBound.doubleValue()) / 6;
        randomDataGenerator = new RandomDataGenerator();
    }

    @Override
    public T mutate(T chromosome) {
        T clonedChromosome = (T) chromosome.cloneGene();
        for (int location = 0; location < clonedChromosome.getGeneLength(); location++) {
            if (RandomUtils.nextDouble() < mutationRate) {
                Double mutatedValue = randomDataGenerator.nextGaussian(clonedChromosome.getGeneAtLocation(location).doubleValue(), standardDeviation);
                clonedChromosome.setGeneAtLocation(location, GeneUtility.clampValues(loweBound, upperBound, (U) mutatedValue));
            }
        }
        return clonedChromosome;
    }
}
