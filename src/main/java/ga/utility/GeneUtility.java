package ga.utility;

import org.apache.commons.math3.random.RandomDataGenerator;

public class GeneUtility {
    private static final double minStartLocation = 0.45;
    private static final double maxStartLocation = 0.75;
    private static final double standardDistance = 0.5;
    private static final RandomDataGenerator randomDataGenerator = new RandomDataGenerator();

    private GeneUtility() {
    }

    public static int generateLocation(int chromosomeLength) {
        int start = (int) (chromosomeLength * minStartLocation);
        int end = (int) (chromosomeLength * maxStartLocation);
        return randomDataGenerator.nextInt(start, end);
    }

    public static int generateLocation(int chromosomeLength, int startLocation) {
        int location = startLocation + (int) (startLocation * standardDistance);
        if (location >= chromosomeLength)
            return chromosomeLength - 1;
        return location;
    }

    public static <T extends Number> T clampValues(T valMin, T valMax, T given) {
        given = (T) (Double) (given.doubleValue() > valMax.doubleValue() ? valMax.doubleValue() : given.doubleValue());
        given = (T) (Double) (given.doubleValue() < valMin.doubleValue() ? valMin.doubleValue() : given.doubleValue());
        return given;
    }
}
